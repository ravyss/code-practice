package ValidParentheses;

import java.util.ArrayDeque;
import java.util.Deque;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

public class ValidParentheses {
	public static void main(String[] args) {

		String input1 = "()";
		String input2 = "()[]{}<Aa>/\\";
		input2 = input2.repeat(999);
		String input3 = "(]";
		String input4 = "([)]";
		String input5 = "()";
		String input6 = "{[]}";

		System.out.println(isBreacketsQueueCorrect(input2));
		
//		System.out.println(checkRightBreacketsStyle(input1));
		System.out.println(checkRightBreacketsStyle(input2));
//		System.out.println(checkRightBreacketsStyle(input3));
//		System.out.println(checkRightBreacketsStyle(input2));
//		System.out.println(checkRightBreacketsStyle(input5));
//		System.out.println(checkRightBreacketsStyle(input6));

	}

	/**
	 * check breacket queue and return true if queue was right
	 * 
	 * @param input
	 * @return
	 */
	static boolean checkRightBreacketsStyle(String input) {
		Long timeStart = System.currentTimeMillis();
		boolean trueBreackets = false;
		int twins = 0;
		Character[] brackets = { '{', '}', '[', ']', '(', ')' ,'A', 'a', '<', '>', '/','\\'};

		if (input.length() % 2 == 0) {
			for (int bracketsType = 0; bracketsType < brackets.length; bracketsType += 2) {
				for (int firstBracket = 0; firstBracket < input.length() - 1; firstBracket++) {
					if (input.charAt(firstBracket) == brackets[bracketsType]) {
						for (int secondBracket = firstBracket + 1; secondBracket < input.length(); secondBracket += 2) {
							if (input.charAt(secondBracket) == brackets[bracketsType + 1]
									& firstBracket % 2 != secondBracket % 2)
								twins++;
						}
					}
				}
			}
		}
		if (input.length() / 2 == twins)
			trueBreackets = true;
		Long timeEnd = System.currentTimeMillis();
		System.out.println(timeEnd - timeStart);
		return trueBreackets;
		
	}

	static boolean isBreacketsQueueCorrect(String input) {
		Long timeStart = System.currentTimeMillis();
		
		Deque<Character> queue  = new ArrayDeque<>();
		Map<Character, Character> bracketMap = new HashMap<>();
		
		bracketMap.put('{','}');
		bracketMap.put('(',')');
		bracketMap.put('[',']');
		bracketMap.put('A', 'a');
		bracketMap.put('<', '>');
		bracketMap.put('/','\\');
		
		for(int i = 0; i < input.length(); i++) {
			Character symbol = input.charAt(i);
			Character closeBracket = bracketMap.get(symbol);
			
			if(closeBracket != null) {
				queue.add(closeBracket);
			} else {
				Character last = queue.pollLast();
				if(symbol != last) {
					Long timeEnd = System.currentTimeMillis();
					System.out.println(timeEnd - timeStart);
					return false;
				}
			}
			
		}
		Long timeEnd = System.currentTimeMillis();
		System.out.println(timeEnd - timeStart);
		return queue.isEmpty();
	}
}
