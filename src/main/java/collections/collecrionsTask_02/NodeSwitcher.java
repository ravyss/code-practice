package collections.collecrionsTask_02;

import java.util.ArrayList;
import java.util.List;

public class NodeSwitcher {
	public void switchFirstAndLastElem(int numberOfSwitchble, Node<String> header) {
		Node<String> frontNode = header.getNextNode();
		Node<String> backNode = header.getPreviousNode();	
		 
		for (int i = 0; i < numberOfSwitchble; i++) {
			frontNode = frontNode.getNextNode();
		}
		for (int i = 0; i < numberOfSwitchble; i++) {
			backNode = backNode.getPreviousNode();
		}
		
		frontNode.setPreviousNode(backNode.getNextNode());
		backNode.setNextNode(frontNode.getPreviousNode());
		
		List<String> elemNodeArray = new ArrayList<>();
		Node<String> loopNodeArray = header.getNextNode();
		while (loopNodeArray.getNextNode() != null) {
			elemNodeArray.add(loopNodeArray.getElement());
			loopNodeArray = loopNodeArray.getNextNode();
		}
		
		System.out.println(elemNodeArray);
	}
}
