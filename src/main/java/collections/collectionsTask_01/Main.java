package collections.collectionsTask_01;

import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Main {
	public static void main(String[] args) {

		Map<String, Person> personsMap = IntStream.range(10, 20)
				.mapToObj(Person::new)
				.sorted((o1, o2) -> o1.getSurname().compareTo(o2.getSurname()))
				.skip(2)
				.filter(person -> person.getSurname().contains("i"))
				.collect(Collectors.toMap(
						person -> person.getSurname(), 
						person -> person, 
						(person1, person2) -> person1));
				
				System.out.println(personsMap);
	}

}
