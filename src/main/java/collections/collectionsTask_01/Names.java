package collections.collectionsTask_01;

public interface Names {
	final static String[] name = { "Liam", "Emma", "Oliver", "Elijah", "Sophia", "Amelia", "Isabella", "Mia", "Evelyn",
			"Alexander" };
	final static String[] secondName = { "Smith", "Jones", "Taylor", "Brown", "Williams", "Wilson", "Johnson", "Davies",
			"Robinson", "Wright" };

}
