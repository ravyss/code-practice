package collections.collectionsTask_01;

import java.util.logging.Logger;

import collections.collectionsTask_05.PersonException;

public class Person{
	private long id;
	private short age;
	private String firstName;
	private String surname;
	private static Logger log = Logger.getLogger(Person.class.getName());
	public static final Person NULL_PERSON = new Person(-1L, (short)-1, "nonExistName", "nonExistSurname");

	/**
	 * @param id
	 * @param age
	 * @param firstName
	 * @param surname
	 */
	private Person(long id, short age, String firstName, String surname) {
		super();
		
		this.id = id;
		this.age = age;
		this.firstName = firstName;
		this.surname = surname;
	}
	
	private static Person checkPerson(Person person) throws PersonException {
		if(person.getId() < 0 || person.getAge() < 0) { 
			throw new PersonException(person);
		}	
		if(person.getFirstName() == null || person.getSurname() == null) {
			throw new PersonException(person);
		}
		
		return person;
	}
	
	public static Person createPerson(long id, short age, String firstName, String surname) {
		Person person = new Person(id,age,firstName,surname);
		
		try {
			return checkPerson(person);
		} catch (PersonException e) {
			log.info(e.getMessage());
		}
		
		
		return NULL_PERSON;
	}
	
	
	
	public Person(long ID) {
		super();
		this.id = ID;
		this.age = ((short) (18 + (Math.random() * 82)));
		this.firstName = Names.name[(int) (Math.random() * 9)];
		this.surname = Names.secondName[(int) (Math.random() * 9)];
	}

	public long getId() {
		return id;
	}

	public short getAge() {
		return age;
	}

	public String getFirstName() {
		return firstName;
	}

	public String getSurname() {
		return surname;
	}

    @Override
    public String toString()
    {
		
        return "id - " + String.valueOf (id) + ", Name: " + firstName + " " +  surname 
        		+ ", age - " + String.valueOf (age);
    }

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + age;
		result = prime * result + ((firstName == null) ? 0 : firstName.hashCode());
		result = prime * result + (int) (id ^ (id >>> 32));
		result = prime * result + ((surname == null) ? 0 : surname.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Person other = (Person) obj;
		if (age != other.age)
			return false;
		if (firstName == null) {
			if (other.firstName != null)
				return false;
		} else if (!firstName.equals(other.firstName))
			return false;
		if (id != other.id)
			return false;
		if (surname == null) {
			if (other.surname != null)
				return false;
		} else if (!surname.equals(other.surname))
			return false;
		return true;
	}
	

}