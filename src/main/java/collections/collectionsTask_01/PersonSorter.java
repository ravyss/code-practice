package collections.collectionsTask_01;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class PersonSorter{
	
	public static void sort(List<Person> list) {
		Collections.sort(list, (o1, o2) -> {
			
			String surname1 = o1.getSurname();
			String surname2 = o2.getSurname();
			String name1 = o1.getFirstName();
			String name2 = o2.getFirstName();

			int result = name2.compareTo(name1);

			if (result == 0) {
				return surname2.compareTo(surname1);
			}

			return result;

		});
	}

	public static void surnameSort(List<Person> list) {
		Collections.sort(list, (o1, o2) -> {
			
			String surname1 = o1.getSurname();
			String surname2 = o2.getSurname();

			return surname1.compareTo(surname2);

		});
	}	
	
}
