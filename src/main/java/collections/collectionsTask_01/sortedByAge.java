package collections.collectionsTask_01;

import java.util.Comparator;

public class sortedByAge implements Comparator<Person>{

	@Override
	public int compare(Person o1, Person o2) {
		short age1 = o1.getAge();
        short age2 = o2.getAge();

        if (age1 > age2) {
            return 1;
        } else if (age1 < age2) {
            return -1;
        } else {
            return 0;
        }
	}
}
