package collections.collectionsTask_01;

import java.util.Comparator;

public class sortedByNAme implements Comparator<Person>{

	@Override
	public int compare(Person o1, Person o2) {
      String str1 = o1.getFirstName();
      String str2 = o2.getFirstName();
          
      return str2.compareTo(str1);
	}

}
