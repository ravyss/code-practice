package collections.collectionsTask_03;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

public class ReversiveIterator<E> implements Iterator<E> {
	private int reverse;
	private List<E> listReverseCollection;

	/**
	 * Creats reverse value for reversing iterator. Iterating from the tail to the
	 * head of this collection.
	 * 
	 * @param reverseCollection
	 */
	public ReversiveIterator(Collection<E> reverseCollection) {
		super();
		listReverseCollection = new ArrayList<>(reverseCollection);
		reverse = listReverseCollection.size() - 1;
	}

	/**
	 * Returns previous element.
	 */
	public E next() {
		return listReverseCollection.get(reverse--);
	}

	@Override
	public boolean hasNext() {
		return reverse >= 0;
	}
}
