package collections.collectionsTask_04;

import java.util.HashMap;
import java.util.Map;

public class GeneralMaps {
	public static void main(String[] args) {

		Map<Integer, String> mainMap = new HashMap<>();
		mainMap.put(1, "one");
		mainMap.put(2, "two");
		mainMap.put(3, "three");
		mainMap.put(4, "four");

		Map<Integer, String> minorMap = new HashMap<>();
		minorMap.put(5, "five");
		minorMap.put(2, "two");
		minorMap.put(6, "six");
		minorMap.put(4, "four");

		Maps newMap = new Maps(mainMap);

		newMap.addIds(minorMap);
		System.out.println(newMap.getName(7));
		System.out.println(newMap.getName(5));
		System.out.println(newMap.isIdsContainsID(6));
		System.out.println(newMap.getIds());
		newMap.setIds(minorMap);
		System.out.println(newMap.getName(1));
		System.out.println(newMap.getName(5));
		System.out.println(newMap.isIdsContainsID(7));
		System.out.println(newMap.getIds());

	}
}
