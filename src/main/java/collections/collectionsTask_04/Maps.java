package collections.collectionsTask_04;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.Collections;

public class Maps {
	private Map<Integer, String> ids;
	private Set<Entry<Integer, String>> entry;

	/**
	 * @param ids
	 */
	public Maps(Map<Integer, String> ids) {
		super();
		this.ids = new HashMap<Integer, String>(ids);
	}

	/*
	 * add new element to ids exept same element
	 */
	public void addIds(Map<Integer, String> idsAnother) {
		for (Entry<Integer, String> entry : idsAnother.entrySet()) {
			ids.putIfAbsent(entry.getKey(), entry.getValue());
		}
	}

	/*
	 * check if ids contains ID
	 */
	public boolean isIdsContainsID(Integer ID) {
		return ids.containsKey(ID);
	}

	/*
	 * got element Name by ID
	 */
	public String getName(Integer ID) {
		return ids.get(ID);
	}

	/*
	 * Returns unmodifiable map - ids.
	 */
	public Map<Integer, String> getIds() {		
		return Collections.unmodifiableMap(ids);
	}

	/*
	 * set map
	 */
	public void setIds(Map<Integer, String> ids) {
		this.ids = ids;
	}

}
