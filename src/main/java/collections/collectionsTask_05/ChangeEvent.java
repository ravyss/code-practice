package collections.collectionsTask_05;

import collections.collectionsTask_01.Person;

public class ChangeEvent {
	private Person beforeChangePerson;
	private Person afterChangePerson;
	private TypeChange type;
	
	/**
	 * @param beforeChangePerson
	 * @param afterChangePerson
	 */
	public ChangeEvent(Person beforeChangePerson, Person afterChangePerson, TypeChange type) {
		super();
		this.beforeChangePerson = beforeChangePerson;
		this.afterChangePerson = afterChangePerson;
		this.type = type;
		
	}
	
	public Person getBeforeChangePerson() {
		return beforeChangePerson;
	}
	
	public Person getAfterChangePerson() {
		return afterChangePerson;
	}
	

	public TypeChange getType() {
		return type;
	}
	
	
	public enum TypeChange{
		Add("added"),
		Remove("removed"),
		Update("updated");
		
		private String type;
		TypeChange(String type) {
			this.type = type;
		}
		
		public String getNameOFType() {
			return type;
		}
		
	}
}
