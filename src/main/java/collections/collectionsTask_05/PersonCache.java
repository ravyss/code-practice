package collections.collectionsTask_05;

import java.util.HashMap;
import java.util.Map;

import collections.collectionsTask_01.Person;

public class PersonCache {

	private Map<Long, Person> cachePersons = new HashMap<>();

	boolean addPerson(Person person) {
		if (person == null)
			return false;

		Person addedPerson = cachePersons.putIfAbsent(person.getId(), person);
		return addedPerson == null;
	}

	Person getPersonById(Long id) {
		Person personById = cachePersons.get(id);
		
		return personById == null ? Person.NULL_PERSON : personById;
	}

	boolean removePersonById(Long id) {
		Person removedPerson = cachePersons.remove(id);
		return removedPerson != null;
	}

}
