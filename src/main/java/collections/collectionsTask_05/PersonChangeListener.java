package collections.collectionsTask_05;

public interface PersonChangeListener {
	void change(ChangeEvent change);
}
