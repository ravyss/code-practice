package collections.collectionsTask_05;

import collections.collectionsTask_01.Person;
import collections.collectionsTask_05.ChangeEvent.TypeChange;

public class PersonChangeListenerImpl implements PersonChangeListener{
	
	private PersonMgr mngImp;
	private int counter;
	
	public PersonChangeListenerImpl(PersonMgr meneger) {
		
		this.mngImp = meneger;
		if(meneger == null) {
			throw new IllegalArgumentException("Meneger can't be null");
		}
		meneger.addListener(this);
		
		
	}

	@Override
	public void change(ChangeEvent change) {
		
		boolean addType = TypeChange.Add == change.getType();
		boolean removeType = TypeChange.Remove == change.getType();
		boolean updateType = TypeChange.Update == change.getType();
		
		if(addType || removeType || updateType) {		
			TypeChange typeAdd = change.getType();
			
			System.out.println("Person was " + typeAdd.getNameOFType() + " #" + counter++);
		}
		
	}
}
