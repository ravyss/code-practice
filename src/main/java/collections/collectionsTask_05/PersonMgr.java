package collections.collectionsTask_05;

import java.util.ArrayList;
import java.util.List;
import java.util.function.BiFunction;

import collections.collectionsTask_01.Person;
import collections.collectionsTask_05.ChangeEvent.TypeChange;

//publisher
public class PersonMgr implements PersonChangeNotifier, PersonService {
	private List<PersonChangeListener> listeners = new ArrayList<>();
	private PersonService personService;

	/**
	 * @param personService
	 */
	public PersonMgr(PersonService personService) {
		super();
		if(personService == null) {
			throw new IllegalArgumentException("Person service can't be null");
		}
		this.personService = personService;
	}

	// notify
	@Override
	public void fireChange(ChangeEvent change) {

		for (PersonChangeListener personChangeListener : listeners) {
			personChangeListener.change(change);
		}
	}

	@Override
	public boolean addListener(PersonChangeListener listener) {

		if (listener != null) {
			listeners.add(listener);
			return true;
		}
		return false;

	}

	@Override
	public boolean removeListener(PersonChangeListener listener) {

		if (listener != null) {
			listeners.remove(listener);
			return true;
		}
		return false;

	}

	@Override
	public boolean addPerson(Person person) {	
		boolean isAdded = personService.addPerson(person);
		if(isAdded) {
			fireChange(new ChangeEvent(Person.NULL_PERSON, person, TypeChange.Add));
		}
		return isAdded;
	}

	@Override
	public Person getPersonById(long id) {
		return personService.getPersonById(id);
	}

	@Override
	public boolean removePersonById(long id) {
		Person person = personService.getPersonById(id);
		boolean isRemoved = personService.removePersonById(id);
		if(isRemoved ) {
			fireChange(new ChangeEvent(person, Person.NULL_PERSON, TypeChange.Remove));
		}
		return isRemoved ;
	}

	@Override
	public boolean changeAge(long id, short age) {
		if(age < 0) return false;
		boolean isChanged = change(personService::changeAge, id, age);

		return isChanged;
	}

	@Override
	public boolean changeName(long id, String name) {
		if(name == null) return false;

		boolean isChanged = change(personService::changeName, id, name);

		return isChanged;
	}

	@Override
	public boolean changeSurname(long id, String surname) {
		
		boolean isChanged = change(personService::changeSurname, id, surname);

		return isChanged;
	}
	
	
	private <T> boolean change(BiFunction<Long,T,Boolean> biFunction, Long id, T value) {
		Person personBefore = personService.getPersonById(id);
		
		if(personBefore.equals(Person.NULL_PERSON)) return false;
		
		boolean isChanged = biFunction.apply(id, value);
		Person personAfter = personService.getPersonById(id);
		
		if(isChanged) {
			fireChange(new ChangeEvent(personBefore, personAfter, TypeChange.Update));
		}
		
		return isChanged;
	}

}
