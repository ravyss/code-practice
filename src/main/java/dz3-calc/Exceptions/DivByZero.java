package Exceptions;

public class DivByZero extends ArithmeticException {

	public DivByZero() {
		super("Can't devide by zero!");
	}

}
