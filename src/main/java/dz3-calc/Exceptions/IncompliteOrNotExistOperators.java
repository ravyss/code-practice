package Exceptions;

public class IncompliteOrNotExistOperators extends RuntimeException {

	public IncompliteOrNotExistOperators(String message) {
		super(message);
	}
}
