import java.util.Scanner;

import Operation.SimpleCalc;

public class Main {
	public static void main(String[] args) {
		Scanner inner = new Scanner(System.in);
		String expression = inner.nextLine();

		SimpleCalc calc = new SimpleCalc(expression);

		try {
			System.out.println(calc.calculateResult());
		} catch (Exception e) {
			e.printStackTrace();
		}
		inner.close();

	}
}
