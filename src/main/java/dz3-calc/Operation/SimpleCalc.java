package Operation;

import Exceptions.DivByZero;
import Exceptions.IncompliteOrNotExistOperators;
import Exceptions.SequenceIsBroken;
import Exceptions.UncompliteCalculation;
import Operators.Operation;

public class SimpleCalc {
	private String expression;

	/**
	 * @param expression
	 */
	public SimpleCalc(String expression) {
		this.expression = expression;
	}

	public double calculateResult() throws UncompliteCalculation {
		double result = 0;
		Splitter split = new Splitter(expression);

		try {
			String[][] expressionData = split.getValuesAndOperatorsInArray();

			wichSymbolsWasRemoved(expressionData);

			result = calculation(expressionData[1][0], Double.parseDouble(expressionData[0][0]),
					Double.parseDouble(expressionData[0][1]));

			for (int i = 1; i < expressionData[1].length - 1; i++) {
				result = calculation(expressionData[1][i], result, Double.parseDouble(expressionData[0][i + 1]));
			}
		} catch (IncompliteOrNotExistOperators | SequenceIsBroken | DivByZero e) {
			e.printStackTrace();
			throw new UncompliteCalculation();
		}
		return result;
	}

	private double calculation(String sigil, double firstOperand, double secondOperand) {
		for (Operation operator : Operation.values()) {
			if (sigil.equals(operator.getOperatorSymbol()))
				return operator.action(firstOperand, secondOperand);
		}
		throw new IncompliteOrNotExistOperators("Operator is not exist!");
	}

	private void wichSymbolsWasRemoved(String[][] expressionData) {
		String clearExpression = "";
		for (String[] strings : expressionData) {
			clearExpression += strings[0];
			clearExpression += strings[1];
		}
		String matches = expression.replaceAll("[" + clearExpression + "]", "");

		if (matches.length() > 0) {
			System.out.println("this expression was cleared from a Unknown Operators! - " + matches);
		}

	}
}
