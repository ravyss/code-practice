package Operators;

import Exceptions.DivByZero;

public enum Operation implements OperationInterface {
	SUM("+") {
		public double action(double firstOperand, double secondOperand) {
			return firstOperand + secondOperand;
		}
	},
	SUB("-") {
		public double action(double firstOperand, double secondOperand) {
			return firstOperand - secondOperand;
		}
	},
	MUL("*") {
		public double action(double firstOperand, double secondOperand) {
			return firstOperand * secondOperand;
		}
	},
	DIV("/") {
		public double action(double firstOperand, double secondOperand) {
			if (secondOperand == 0) {
				throw new DivByZero();
			}

			return firstOperand / secondOperand;
		}
	};

	public abstract double action(double firstOperand, double secondOperand);

	private String operatorSymbol;

	private Operation(String operatorSymbol) {
		this.operatorSymbol = operatorSymbol;
	}

	public String getOperatorSymbol() {
		return operatorSymbol;
	}
}
