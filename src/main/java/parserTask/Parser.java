package parserTask;


public interface Parser<T> {

	T parse(String jsonResponse);
}
