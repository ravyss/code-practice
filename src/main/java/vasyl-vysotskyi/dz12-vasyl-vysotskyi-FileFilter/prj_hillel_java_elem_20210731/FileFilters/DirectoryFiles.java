package prj_hillel_java_elem_20210731.FileFilters;

import java.io.File;
import java.util.List;

public class DirectoryFiles {

	private static File directory;
	private static File[] files;

	public void setDir(String dir) {
		if (new File(dir).isDirectory()) {
			directory = new File(dir);
			files = directory.listFiles();

		} else {
			System.out.println("directory " + dir + " not exist!");
			createNewDir(dir);
		}
	}

	public boolean createNewDir(String dir) {

		directory = new File(dir);
		directory.mkdirs();
		System.out.println("Create new directory - " + dir);
		files = null;
		return directory.exists();
	}

	public void deleteFilesInDir() {
		for (File s : files) {
			s.delete();
		}
		files = directory.listFiles();
	}

	public File[] getFiles() {
		return files;
	}

	public File[] getFilesFromDirectory(String dir) {
		File directory = new File(dir);
		return directory.listFiles();
	}

	public void setFiles(List<File> listOfFiles) {
		files = new File[listOfFiles.size()];
		for (int i = 0; i < listOfFiles.size(); i++) {
			files[i] = listOfFiles.get(i);
		}

	}

}
