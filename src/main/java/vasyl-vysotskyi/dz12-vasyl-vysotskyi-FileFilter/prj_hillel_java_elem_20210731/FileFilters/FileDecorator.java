package prj_hillel_java_elem_20210731.FileFilters;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class FileDecorator extends DirectoryFiles {

	private String[] imgFormat = { ".tif", ".bmp", ".jpg", ".jpeg", ".gif", ".png", ".raw" };

	public File[] getArrayOfImg() {
		List<File> files = new ArrayList<>();
		for (File file : getFiles()) {
			for (String suffix : imgFormat) {
				if (file.getName().toLowerCase().endsWith(suffix)) {
					files.add(file);
				}
			}
		}
		setFiles(files);
		return getFiles();
	}

	public File[] getListOfTheseFormatFiles(String format) {
		List<File> files = new ArrayList<>();
		for (File file : getFiles()) {
			if (file.getName().toLowerCase().endsWith(format)) {
				files.add(file);
			}
		}
		setFiles(files);
		return getFiles();
	}

	public File[] getAllFilesLessThanSize(int size) {
		List<File> files = new ArrayList<>();
		for (File file : getFiles()) {
			if (file.length() <= size) {
				files.add(file);
			}
		}
		setFiles(files);
		return getFiles();
	}

	public File[] getAllFilesMoreThanSize(int size) {
		List<File> files = new ArrayList<>();
		for (File file : getFiles()) {
			if (file.length() >= size) {
				files.add(file);
			}
		}
		setFiles(files);
		return getFiles();
	}

}
