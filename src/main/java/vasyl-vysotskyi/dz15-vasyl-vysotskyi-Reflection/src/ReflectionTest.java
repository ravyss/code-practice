package src;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Objects;
import java.util.Properties;

public class ReflectionTest {
	public static void main(String[] args)
			throws IOException, ClassNotFoundException, NoSuchMethodException, SecurityException,
			InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException {

		final Properties properties = new Properties();
		String resDir = "\\vasyl-vysotskyi\\dz15-vasyl-vysotskyi-Reflection\\resources";
		properties
				.load(Files.newBufferedReader(Paths.get(System.getProperty("user.dir"), resDir, "config.properties")));

		String hashMapName = properties.getProperty("ClassHashMap");
		String keyClassName = properties.getProperty("keyType");
		String valueClassName = properties.getProperty("valueType");

		Class hashMapType = Class.forName(hashMapName);
		Class keyType = Class.forName(keyClassName);
		Class valueType = Class.forName(valueClassName);

		testMap(hashMapType, keyType, valueType);
	}

	private static void testMap(Class mapType, Class keyType, Class valueType)
			throws InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException,
			NoSuchMethodException, SecurityException, ClassNotFoundException {
		Object map = mapType.getConstructor(null).newInstance();
		Integer[] key = { 14, 15, 16, 17, 18 };
		String[] value = { "rabbit", "15", "16", "17", "18" };

		putAll(mapType, map, key, value, keyType, valueType);

		getValueOfKey(mapType, map, key[0]);

		removeCellByKey(mapType, map, key[0]);
		key = new Integer[] { key[1], key[2], key[3], key[4] };

		getAllValue(mapType, map, key);

	}

	private static void putAll(Class mapType, Object map, Object[] key, Object[] value, Class keyType, Class valueType)
			throws IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException,
			SecurityException, ClassNotFoundException {
		for (int i = 0; i < key.length; i++) {
			put(mapType, map, key[i], value[i], keyType, valueType);
		}

	}

	private static void removeCellByKey(Class mapType, Object map, Object key) throws IllegalAccessException,
			IllegalArgumentException, InvocationTargetException, NoSuchMethodException, SecurityException {
		Object valueOfKey = mapType.getMethod("get", Object.class).invoke(map, key);
		if (valueOfKey == null) {
			throw new EmptyMapException("map is empty, there is nothing to remove :(");
		} else {
			mapType.getMethod("remove", Object.class).invoke(map, key);
		}

	}

	private static void put(Class mapType, Object map, Object key, Object value, Class keyType, Class valueType)
			throws IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException,
			SecurityException, ClassNotFoundException {

		boolean isKeyCorrect = Objects.equals(key.getClass(), keyType);
		boolean isValueCorrect = value.getClass() == valueType;

		if (isKeyCorrect && isValueCorrect) {
			mapType.getMethod("put", Object.class, Object.class).invoke(map, key, value);
		} else {
			if (isKeyCorrect) {
				System.out.println("value must be an String classType");
			} else if (isValueCorrect) {
				System.out.println("key must be an Integer classType");
			}

		}

	}

	public static Object getValueOfKey(Class mapType, Object map, Object key)
			throws IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException,
			SecurityException, EmptyMapException {
		Object valueOfKey = mapType.getMethod("get", Object.class).invoke(map, key);
		if (valueOfKey == null) {
			throw new EmptyMapException("map is empty, call the correct put method first");
		} else {
			return valueOfKey;
		}
	}

	public static Object[] getAllValue(Class mapType, Object map, Object[] key)
			throws IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException,
			SecurityException, EmptyMapException {
		Object[] allValue = new Object[key.length];
		for (int i = 0; i < key.length; i++) {
			allValue[i] = getValueOfKey(mapType, map, key[i]);
			System.out.println(allValue[i]);
		}

		return allValue;
	}

	public static class EmptyMapException extends RuntimeException {
		public EmptyMapException(String cause) {
			super(cause);
		}
	}

}
