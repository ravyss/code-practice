package src;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Properties;

import src.annot.Annot1;

public class ReflectSquare {
	public static void main(String[] args) throws IOException, ClassNotFoundException {
		final Properties properties = new Properties();
		String resDir = "\\vasyl-vysotskyi\\dz16-vasyl-vysotskyi-Annotation\\src\\annot\\resources";
		properties
				.load(Files.newBufferedReader(Paths.get(System.getProperty("user.dir"), resDir, "config.properties")));
		
		String sqareName = properties.getProperty("className");

		Class<?> sqareType = Class.forName(sqareName);
		
		Method[] squareMeth = sqareType.getMethods();
		
		for(Method sqrMeth : squareMeth) {
			if(sqrMeth.isAnnotationPresent(Annot1.class)) {
				Annotation ann = (Annot1) sqrMeth.getDeclaredAnnotation(Annot1.class);
				System.out.println(((Annot1) ann).value());
			}
		}
	}
	

}
