package src;

import src.annot.*;

@Ver("snapshot 0.0.0.1")
public class Sqare<T> {
	
	@TopSide
	@MinValue(8)
	private int aSide;

	@MinValue(5)
	@RightSide
	private int bSide;

	@ValueIsFive
	@MinValue(6)
	@BottomSide
	private int cSide;

	@LeftSide
	@MinValue(3)
	private int dSide;

	@Unethical
	@SqareName("ABCD")
	private String name;

	@AllInit
	public Sqare(int topBoarder, int sideBoarder, String name) {
		super();
		this.name = name;
		this.aSide = topBoarder;
		this.bSide = sideBoarder;
		this.cSide = topBoarder;
		this.dSide = sideBoarder;
	}

	/**
	 * 
	 */
	public Sqare() {
		super();
	}

	@Annot1("getPerimeter")
	public int getPerimeter() {
		return aSide * 2 + bSide * 2;
	}

	@Annot1("getSquare")
	public int getSquare() {
		return (int) Math.pow(aSide * bSide, 2.0);
	}

	@Annot1("getaSide")
	public int getaSide() {
		if (aSide == cSide) {
			return aSide;
		} else {
			return aSide = cSide;
		}
	}

	@Annot1("setaSide")
	public void setaSide(int aSide) {
		this.aSide = aSide;
		this.cSide = aSide;
	}

	public int getbSide() {
		if (bSide == dSide) {
			return bSide;
		} else {
			return bSide = dSide;
		}
	}

	public void setbSide(int bSide) {
		this.bSide = bSide;
		this.dSide = bSide;
	}

	public int getcSide() {
		if (aSide == cSide) {
			return cSide;
		} else {
			return cSide = aSide;
		}
	}

	public void setcSide(int cSide) {
		this.aSide = cSide;
		this.cSide = aSide;
	}

	public int getdSide() {
		if (dSide == bSide) {
			return dSide;
		} else {
			return dSide = bSide;
		}
	}

	public void setdSide(int dSide) {
		this.bSide = dSide;
		this.dSide = dSide;
	}

}
