package src;

public class Program_19_02 {
    public static void main(String[] args) {
       Runnable runnable = new Runnable() {
           @Override
           public void run() {
               try {
                   long start = System.nanoTime();
                   Thread.sleep(500);
                   long finish = System.nanoTime();
                   System.out.println((finish - start));
               } catch (InterruptedException e) {
                   e.printStackTrace();
               }
               System.out.println("");
           }
       };
       
       Runnable runnable2 = new Runnable() {
           @Override
           public void run() {
               try {
                   long start = System.nanoTime();
                   Thread.sleep(800);
                   long finish = System.nanoTime();
                   System.out.println((finish - start));
               } catch (InterruptedException e) {
                   e.printStackTrace();
               }
               System.out.println("");
           }
       };
       long start = System.nanoTime();
       runnable.run();
       runnable2.run();
       long finish = System.nanoTime();
       System.out.println("one" + (finish - start));
       
       Thread thread2 = new Thread(runnable2);
       Thread thread = new Thread(runnable);

       
       start = System.nanoTime();
       thread.start();   
       thread2.start();
       finish = System.nanoTime();
       System.out.println("two" + (finish - start));
       
       start = System.nanoTime();
       runnable.run();
       runnable2.run();
       finish = System.nanoTime();
       System.out.println("one" + (finish - start));
    }
}
