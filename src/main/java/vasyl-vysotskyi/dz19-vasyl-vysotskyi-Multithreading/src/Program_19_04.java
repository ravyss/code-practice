package src;
public class Program_19_04 {
    public static void main(String[] args) {
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                for (long i = 0; i < Long.MAX_VALUE; i++) {
                    if (i % 3 == 0) {
                        for (int j = 0; j < 10000; j++) {
                            System.out.println();
                        }
                    } else {
                        Thread.yield();
                    }
                }
            }
        };

        Thread thread = new Thread(runnable);
        thread.start();

        for (long i = 0; i < Long.MAX_VALUE; i++) {
            System.out.println("first");
            if (i == ((long) Long.MAX_VALUE / 2)) {
                thread.interrupt();
            }
        }
    }
}
