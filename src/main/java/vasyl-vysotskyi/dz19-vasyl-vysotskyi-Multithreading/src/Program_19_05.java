package src;
import java.util.ArrayList;
import java.util.List;

public class Program_19_05 {
    static int a = 0;

    public static void main(String[] args) {
        List<Thread> threadList = new ArrayList<>();

        for (int i = 0; i < 100; i++) {
            Thread thread = new Thread(Program_19_05::run);
            thread.setName("My thread - " + i);
            threadList.add(thread);
        }
        for (int i = 0; i < 100; i++) {
            threadList.get(i).start();
        }
        for (int i = 0; i < 100; i++) {
            if (threadList.get(i).isAlive()) {
                try {
                    threadList.get(i).join();
                } catch (InterruptedException e) {
                    throw new IllegalStateException(e);
                }
            }
        }
        System.out.println("Main finished");
    }

    private static void run() {
        for (int i = 0; i < Integer.MAX_VALUE; i++) {
            a++;
            System.out.println(Thread.currentThread().getName());
        }
    }
}
