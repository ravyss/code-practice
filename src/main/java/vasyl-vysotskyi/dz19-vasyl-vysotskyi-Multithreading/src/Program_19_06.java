package src;
public class Program_19_06 {
    static volatile int c = 0;

    public static void main(String[] args) throws InterruptedException {
        int a = 0;

        new Thread(() -> test(args, a)).start();

        test(args, a);
    }

    public static void test(String[] args, int a) {
        int b = a + 10 + c++;
        test(args, b);
    }
}
