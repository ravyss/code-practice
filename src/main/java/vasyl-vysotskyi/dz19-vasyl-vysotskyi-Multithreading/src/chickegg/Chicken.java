package src.chickegg;

public class Chicken extends Thread {
	private long start;
	private long finish;

	public void run() {
		for (int i = 0; i < 5; i++) {
			start = System.nanoTime();
			try {
				sleep(1000);
				System.out.println("Chicken!");
				finish = System.nanoTime();

			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

	public long getTime() {
		return finish - start;
	}
}
