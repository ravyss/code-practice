package src.chickegg;

public class Egg extends Thread {
	long start;
	long finish;

	public void run() {
		for (int i = 0; i < 5; i++) {
			start = System.nanoTime();
			try {
				sleep(1000);

				System.out.println("egg!");
				finish = System.nanoTime();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
	public long getTime() {
		return finish - start;
	}

}
