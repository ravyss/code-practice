package src.chickegg;

public class Main {

	static Chicken chickenVoice;
	static Egg eggVoice;

	public static void main(String[] args) {

		chickenVoice = new Chicken();
		eggVoice = new Egg();

		eggVoice.start();
		chickenVoice.start();

		try {
			eggVoice.join();
			chickenVoice.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		System.out.println(eggVoice.getTime() > chickenVoice.getTime() ? "Chicken is First!" : "Egg is First!");

	}

}
