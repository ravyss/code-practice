package src;

import java.util.concurrent.CountDownLatch;

public class CDLDemo {
	public static void main(String[] args) {
		CountDownLatch cdl = new CountDownLatch(5);
		System.out.println("Start thread");

		new MyThread(cdl);

		try {
			cdl.await();
		} catch (InterruptedException exc) {
			System.out.println(exc);
		}

		System.out.println("End thread");

	}
}


