package src;

import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;

public class CyclBar {
	public static void main(String[] args) {
		CyclicBarrier cbr = new CyclicBarrier(3, new Action());
		System.out.println("Start thread");

		new MyCBThread(cbr);
		new MyCBThread(cbr);
		new MyCBThread(cbr);

		System.out.println("End thread");

	}
}

class MyCBThread implements Runnable {
	CyclicBarrier parties;

	public MyCBThread(CyclicBarrier c) {
		parties = c;
		new Thread(this).start();
	}

	@Override
	public void run() {
		System.out.println(Thread.currentThread().getName());
		try {
			parties.await();
		} catch (InterruptedException e) {
			e.printStackTrace();
		} catch (BrokenBarrierException e) {
			e.printStackTrace();
		}
	}
}

class Action implements Runnable {

	@Override
	public void run() {
		System.out.println("its on barier!");
	}
}