package src;

public class Dz_21_Multithreading {
	public static void main(String[] args) {
		Message msg  = new Message("get work");
		Waiter wtr = new Waiter(msg);
		new Thread(wtr, "waiter").start();
		
		Waiter wtr1 = new Waiter(msg);
		new Thread(wtr1, "waiter1").start();
		
		Notifier ntfr = new Notifier(msg);
		new Thread(ntfr, "notifier").start();
		
		System.out.println("All threads is started!");
	}
}
