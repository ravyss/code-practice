package src;

public class JavaAtomic {

	public static void main(String[] args) throws InterruptedException {
		ProcessingThreadWithoutAtomic pt = new ProcessingThreadWithoutAtomic();
		ProcessingThreadWithAtomic ptat = new ProcessingThreadWithAtomic();
		
		Thread t1A = new Thread(ptat, "t1A");
		Thread t1 = new Thread(pt,"t1");
		t1A.start();
		t1.start();
		
		Thread t2A =  new Thread(ptat, "t2A");
		Thread t2 = new Thread(pt, "t2");
		t2A.start();
		t2.start();
		
		t1A.join();
		t1.join();
		t2A.join();
		t2.join();
		
		System.out.println("Processing without Atomic count=" + pt.getCount());
		System.out.println("Processing Atomic count=" + ptat.getCount());

	}

}
