package src;

import java.util.concurrent.CountDownLatch;

public class MyThread implements Runnable {
	CountDownLatch latch;

	public MyThread(CountDownLatch c) {
		latch = c;
		new Thread(this).start();
	}

	@Override
	public void run() {
		for (int i = 0; i < 4; i++) {
			System.out.println(i);
			latch.countDown();
		}
		System.out.println("last");
		latch.countDown();
	}
}
