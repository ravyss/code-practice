package src;

public class Notifier implements Runnable {

	private Message msg;

	public Notifier(Message msg) {
		super();
		this.msg = msg;
	}

	@Override
	public void run() {
		String name = Thread.currentThread().getName();
		System.out.println(name + " started");
		try {
			Thread.sleep(1000);
			synchronized (msg) {
				msg.setMsg(name + " thread Notifier get work");
				msg.notify();
			}
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

	}

}
