package src;

import java.util.concurrent.Phaser;

public class PhaseThread implements Runnable {
	Phaser phsr;

	public PhaseThread(Phaser phsr) {
		super();
		this.phsr = phsr;
		new Thread(this).start();
		phsr.register();
	}

	@Override
	public void run() {
		System.out.println(Thread.currentThread().getName());
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		phsr.arriveAndAwaitAdvance();
		
	}

}
