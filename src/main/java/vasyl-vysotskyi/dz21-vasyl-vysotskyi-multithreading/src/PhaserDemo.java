package src;

import java.util.concurrent.Phaser;

public class PhaserDemo {

	public static void main(String[] args) {
		Phaser phsr = new Phaser(0);

		new PhaseThread(phsr);
		int phase = phsr.getPhase();
		phsr.arriveAndAwaitAdvance();
		System.out.println(phase + " is done");

		new PhaseThread(phsr);

		int phase1 = phsr.getPhase();
		phsr.arriveAndAwaitAdvance();
		System.out.println(phase1 + " is done");

		new PhaseThread(phsr);
		int phase2 = phsr.getPhase();
		System.out.println(phase2 + " is done");
		
		System.out.println(phsr.arriveAndDeregister() + " end threads");

	}

}

