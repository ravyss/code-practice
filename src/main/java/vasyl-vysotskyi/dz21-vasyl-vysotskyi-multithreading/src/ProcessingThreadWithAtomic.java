package src;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

public class ProcessingThreadWithAtomic implements Runnable {
	private final AtomicInteger count = new AtomicInteger();

	@Override
	public void run() {
		for(int i = 0; i<5;i++) {
			processSmthng(i);
			count.incrementAndGet();
			System.out.format("%s: %d\ntime:%d\n", 
					Thread.currentThread().getName(), count.get(), System.currentTimeMillis());
		}
	}

	public AtomicInteger getCount() {
		return count;
	}
	
	private void processSmthng(int i) {
		try {
			TimeUnit.SECONDS.sleep(i);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

}
