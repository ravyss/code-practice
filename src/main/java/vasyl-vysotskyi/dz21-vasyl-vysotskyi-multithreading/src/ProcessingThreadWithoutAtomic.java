package src;

import java.util.concurrent.TimeUnit;

public class ProcessingThreadWithoutAtomic implements Runnable {
	private int count;

	@Override
	public void run() {
		for (int i = 0; i < 5; i++) {
			processSomething(i);
			count++;
			System.out.format("%s: %d\ntime:%d\n", 
					Thread.currentThread().getName(), count, System.currentTimeMillis());
		}

	}
	
	public int getCount() {
		return this.count;
	}
	
	private void processSomething(int i) {
		try {
			TimeUnit.SECONDS.sleep(i);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

}
