package src;

public class Waiter implements Runnable{
	
	private Message msg;
	
	public Waiter(Message m) {
		super();
		this.msg = m;
	}


	@Override
	public void run() {
		String name = Thread.currentThread().getName();
		synchronized (msg) {
			try {
				System.out.println(name + 
						" waiting for the notify method to be called: " + 
						System.currentTimeMillis());
				msg.wait();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			
			System.out.println(name + 
					" there was a call to the notify method: " + 
					System.currentTimeMillis());
			System.out.println(name + " : " + msg.getMsg());
		}
		
	}
}
