package src.main.java;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class CreateFiles {

	public void selectAndCopy(File[] files, File directory, File ready, String companyName) throws IOException {

		for (File file : files) {
			File source = new File(directory, file.getName());
			File dest = new File(ready, companyName + file.getName());
			copyFileUsingStream(source, dest);
		}

	}

	private static void copyFileUsingStream(File source, File dest) throws IOException {
		InputStream is = null;
		OutputStream os = null;
		try {
			is = new FileInputStream(source);
			os = new FileOutputStream(dest);
			byte[] buffer = new byte[1024];
			int length;
			while ((length = is.read(buffer)) > 0) {
				os.write(buffer, 0, length);
			}
		} finally {
			is.close();
			os.close();
		}
	}
}
