package src.main.java;

import java.io.File;

import org.apache.commons.lang3.ArrayUtils;

public class DirectoryFiles {
	String userDir = System.getProperty("user.dir");
	private final File SOURCE = new File(userDir + "\\src\\main\\resources");
	private final File DEST = new File(userDir + "\\src\\main\\ReadyFiles");
	final File VARIBLE = new File(SOURCE.getPath() + "\\" + "varible.txt");

	public File[] getFiles(File dir) {
		File[] dirFile = dir.listFiles();

		for (int i = 0; i < dirFile.length; i++) {
			if (dirFile[i].equals(VARIBLE)) {
				dirFile = ArrayUtils.removeElement(dirFile, VARIBLE);
			}
		}
		return dirFile;
	}

	public File[] getSourceFiles() {
		return getFiles(SOURCE);
	}

	public File[] getDestFiles() {
		return getFiles(DEST);
	}

	public File getSource() {
		return SOURCE;
	}

	public File getDest() {
		return DEST;
	}

}
