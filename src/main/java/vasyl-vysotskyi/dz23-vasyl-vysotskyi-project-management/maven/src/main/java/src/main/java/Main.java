package src.main.java;

import java.io.IOException;

public class Main {

	public static void main(String[] args) throws IOException {

		String s = System.getProperty("user.dir");
		CreateFiles f = new CreateFiles();

		Replacer r = new Replacer();

		DirectoryFiles d = new DirectoryFiles();

		f.selectAndCopy(d.getSourceFiles(), d.getSource(), d.getDest(), "Comp1");

		r.replacer(d.getSource(), d.getDestFiles(), d.getDest());

	}

}
