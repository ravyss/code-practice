package src.main.java;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.poi.xwpf.usermodel.XWPFRun;
import org.apache.poi.xwpf.usermodel.XWPFTable;
import org.apache.poi.xwpf.usermodel.XWPFTableCell;
import org.apache.poi.xwpf.usermodel.XWPFTableRow;

public class Replacer {

	public void replacer(File directVarible, File[] files, File ready) throws IOException {

		File varible = new File(directVarible, "varible.txt");

		ArrayList<String> list = new ArrayList<>();
		try (Scanner scan = new Scanner(varible)) {
			while (scan.hasNextLine()) {
				list.add(scan.nextLine());
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}

		String[] arrayOfVarible = list.toArray(new String[0]);
		String[] replacmentVarible = new String[arrayOfVarible.length];

		Scanner inner = new Scanner(System.in);

		for (int i = 0; i < arrayOfVarible.length; i++) {

			System.out.println(i + ".\t" + arrayOfVarible[i]);
			replacmentVarible[i] = inner.next();
		}

		for (int i = 0; i < files.length; i++) {
			File file = new File(ready + "\\" + files[i].getName());
			FileInputStream fis = new FileInputStream(file.getAbsolutePath());
			XWPFDocument doc = new XWPFDocument(fis);
			for (XWPFParagraph p : doc.getParagraphs()) {
				List<XWPFRun> runs = p.getRuns();
				if (runs != null) {
					for (XWPFRun r : runs) {
						String text = r.getText(0);
						for (int z = 0; z < arrayOfVarible.length; z++) {
							if (text != null && text.contains(arrayOfVarible[z])) {
								text = text.replace(arrayOfVarible[z], replacmentVarible[z]);
								r.setText(text, 0);
							}
						}
					}
				}
			}
			for (XWPFTable tbl : doc.getTables()) {
				for (XWPFTableRow row : tbl.getRows()) {
					for (XWPFTableCell cell : row.getTableCells()) {
						for (XWPFParagraph p : cell.getParagraphs()) {
							for (XWPFRun r : p.getRuns()) {
								String text = r.getText(0);
								for (int z = 0; z < arrayOfVarible.length; z++) {
									if (text != null && text.contains(arrayOfVarible[z])) {
										text = text.replace(arrayOfVarible[z], replacmentVarible[z]);
										r.setText(text, 0);
									}
								}
							}
						}
					}
				}
			}
			doc.write(new FileOutputStream(ready + "\\" + files[i].getName()));
		}
	}

}
