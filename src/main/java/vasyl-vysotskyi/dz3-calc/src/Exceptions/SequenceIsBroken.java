package Exceptions;

public class SequenceIsBroken extends RuntimeException {

	public SequenceIsBroken() {
		super("the sequence of operators is broken!");
	}

}
