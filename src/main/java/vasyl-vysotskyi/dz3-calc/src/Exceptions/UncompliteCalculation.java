package Exceptions;

public class UncompliteCalculation extends Exception {

	public UncompliteCalculation() {
		super("Calculation is not complite!");
	}
}
