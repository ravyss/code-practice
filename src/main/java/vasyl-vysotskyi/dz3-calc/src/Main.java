
import Operation.SimpleCalc;

public class Main {
	public static void main(String[] args) {
		String expression = String.join(" ", args);

		SimpleCalc calc = new SimpleCalc(expression);

		try {
			System.out.println(calc.calculateResult());
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
}
