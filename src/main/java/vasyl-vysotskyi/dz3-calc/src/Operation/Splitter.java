package Operation;

import Exceptions.IncompliteOrNotExistOperators;
import Exceptions.SequenceIsBroken;
import Operators.Operation;

public class Splitter {
	private String expression;
	private String exeptUsableCharacters = "";

	/**
	 * @param expression
	 */
	public Splitter(String expression) {
		this.expression = expression;
	}

	public String[][] getValuesAndOperatorsInArray() {
		String clearExpression = cleaner();
		int numOfValues = calculatedNumberOfValues(clearExpression);
		String[][] valueAndOperators = splitValueAndOperators(numOfValues, clearExpression);

		return valueAndOperators;
	}

	private int calculatedNumberOfValues(String clearExpression) {
		int numOfValues = 0;
		char[] fragmentedExpression = clearExpression.toCharArray();

		for (char c : fragmentedExpression) {
			if (Character.isDigit(c) || c == '.') {
			} else {
				numOfValues++;
			}
		}

		return ++numOfValues;
	}

	private String cleaner() {

		for (Operation operator : Operation.values()) {
			exeptUsableCharacters += "\\" + operator.getOperatorSymbol();
		}
		exeptUsableCharacters = "[^0-9." + exeptUsableCharacters + "]";

		String clearExpression = expression.replaceAll(exeptUsableCharacters, "");

		boolean isTheSequenceOfTheExpressionIncorrect = !Character.isDigit(clearExpression.toCharArray()[0])
				|| !Character.isDigit(clearExpression.toCharArray()[clearExpression.length() - 1]);
		if (isTheSequenceOfTheExpressionIncorrect) {
			throw new SequenceIsBroken();
		}

		return clearExpression;
	}

	private String[][] splitValueAndOperators(int numOfDigit, String clearExpression) {
		String[][] expressionData = new String[2][numOfDigit];
		char[] fragmentedExpression = clearExpression.toCharArray();

		int i = 0;
		for (char c : fragmentedExpression) {
			if (Character.isDigit(c) || c == '.') {
				expressionData[0][i] += Character.toString(c);
			} else {
				expressionData[1][i] = Character.toString(c);
				i++;
			}
		}

		boolean isExpressionIncomplite = expressionData[0].length < 2 || expressionData[1].length < 1;
		if (isExpressionIncomplite) {
			throw new IncompliteOrNotExistOperators("Incomplite expression!");
		}

		i = 0;
		for (String strings : expressionData[0]) {
			expressionData[0][i] = strings.replaceAll(exeptUsableCharacters, "");
			i++;
		}

		return expressionData;
	}

}
