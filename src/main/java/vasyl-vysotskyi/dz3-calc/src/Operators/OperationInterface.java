package Operators;

public interface OperationInterface {
	double action(double firstOperand, double secondOperand);

	String getOperatorSymbol();

}
