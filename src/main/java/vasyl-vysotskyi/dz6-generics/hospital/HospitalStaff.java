package hospital;

import java.util.ArrayList;
import java.util.List;

import hospital.staff.Director;
import hospital.staff.Doctor;
import hospital.staff.Nurse;
import hospital.staff.hierarchy.Person;

public class HospitalStaff {
	public static void main(String[] args) {
		List<Person> staff = new ArrayList<>();

		Nurse nurse = new Nurse();
		nurse.setEmployee(23, "NurseName");

		staff.add(nurse.getEmployee());

		System.out.println(staff.get(0).getPosition());
		System.out.println(staff.get(0).getName());
		System.out.println(staff.get(0).getAge());
		
		nurse.action();
	}
}
