package hospital.staff;

import hospital.staff.hierarchy.Employee;
import hospital.staff.hierarchy.Person;

public class Director implements Employee<Person> {
	private final String POSITION = "Nurse";
	private Person director;

	@Override
	public void setEmployee(int age, String name) {
		director = new Person(age, name);
		director.setPosition(POSITION);
	}

	@Override
	public Person getEmployee() {
		return director;
	}

	@Override
	public void action() {
		System.out.println(POSITION + " do action!");
		
	}

}
