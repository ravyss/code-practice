package hospital.staff;

import hospital.staff.hierarchy.Employee;
import hospital.staff.hierarchy.Person;

public class Doctor implements Employee<Person> {
	private final String POSITION = "Nurse";
	private Person doctor;

	@Override
	public void setEmployee(int age, String name) {
		doctor = new Person(age, name);
		doctor.setPosition(POSITION);
	}

	@Override
	public Person getEmployee() {
		return doctor;
	}

	@Override
	public void action() {
		System.out.println(POSITION + " do action!");
		
	}
}
