package hospital.staff;

import hospital.staff.hierarchy.Employee;
import hospital.staff.hierarchy.Person;

public class Nurse implements Employee<Person> {
	private final String POSITION = "Nurse";
	private Person nurse;

	@Override
	public void setEmployee(int age, String name) {
		nurse = new Person(age, name);
		nurse.setPosition(POSITION);
	}

	@Override
	public Person getEmployee() {
		return nurse;
	}

	@Override
	public void action() {
		System.out.println(POSITION + " do action!");		
	}
}