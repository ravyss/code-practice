package hospital.staff.hierarchy;

public interface Employee<P extends Person> {

	void setEmployee(int age, String name);

	P getEmployee();

	void action();

}
