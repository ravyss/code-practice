package prj_hillel_java_elem_20210731;

import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Objects;

public class TheBestArrayList<T> implements List<T> {
	private static final int DEFAULT_CAPACITY = 10;
	private T[] arr;
	private int count = 0;

	public TheBestArrayList() {
		this(DEFAULT_CAPACITY);
	}

	@SuppressWarnings("unchecked")
	public TheBestArrayList(int capacity) {
		if (capacity >= 0) {
			arr = (T[]) new Object[capacity];
			this.count = capacity;
		} else {
			throw new IllegalArgumentException("Illegal capacity value: " + capacity);
		}
	}

	@Override
	public int size() {
		return count;
	}

	@Override
	public boolean isEmpty() {
		return count == 0;
	}

	@Override
	public boolean contains(Object o) {
		for (Object obj : arr) {
			if (Objects.equals(obj, o)) {
				return true;
			}
		}
		return false;
	}

	@Override
	public Iterator<T> iterator() {
		return new Iterator<>() {
			int cursor = -1;

			public boolean hasNext() {
				return cursor + 1 < count;
			}

			public T next() {
				return arr[++cursor];
			}
		};
	}

	@Override
	public Object[] toArray() {
		return Arrays.copyOf(arr, count);
	}

	@Override
	public <E> E[] toArray(E[] a) {
		return (E[]) Arrays.copyOf(arrElement, size);
	}

	@Override
	public boolean add(T e) {
		if (!isEmpty()) {
			for (int i = 0; i < arr.length; i++) {
				if (Objects.equals(arr[i], null)) {
					arr[i] = e;
					return true;
				}
			}
		}
		int oldCount = count;
		count = (count*3)/2+1;
		Object[] addToArr = new Object[count];
		for (int i = 0; i < arr.length; i++) {
			addToArr[i] = arr[i];
		}
		addToArr[oldCount - 1] = e;
		arr = (T[]) addToArr;
		return true;
	}

	@Override
	public boolean remove(Object o) {
		for (int i = 0; i < arr.length; i++) {
			if (Objects.equals(arr[i], o)) {
				arr[i] = null;
				return true;
			}
		}
		return false;
	}

	@Override
	public boolean containsAll(Collection<?> c) {
		boolean isContain = true;
		for (Object obj : c) {
			isContain &= contains(obj);
			if (!contains(obj)) {
				System.out.println("object " + obj + " not contains");
			}
		}
		return isContatin;
	}

	@Override
	public boolean addAll(Collection<? extends T> c) {
		for (Object obj : c) {
			add(obj);
		}
		return true;
	}

	@Override
	public boolean addAll(int index, Collection<? extends T> c) {
		for (Object obj : c) {
			add(index++, obj);
		}
		return true;
	}

	@Override
	public boolean removeAll(Collection<?> c) {
		boolean isRemoved = true;
		for (Object obj : c) {
			isRemoved &= remove(obj);
			if (!remove(obj)) {
				System.out.println("object " + obj + " not removed!");
			}
		}
		return isRemoved;
	}

	@Override
	public boolean retainAll(Collection<?> c) {
		private int cursor = 0; 
		T[] newArr = (T[]) new Object[count];
		for(Object cObj : c) {
			if(contains(cObj)) {
				newArr[cursor++] = cObj;
			}
		}
		arr = newArr;
		return true;
		
	}

	@SuppressWarnings("unchecked")
	@Override
	public void clear() {
		arr = (T[]) new Object[count];
	}

	@Override
	public T get(int index) {
		indexCheck(index);
		return arr[index];
	}

	@Override
	public T set(int index, T e) {
		indexCheck(index);
		T oldValue = arr[index];
		arr[index] = e;
		return oldValue;
	}

	@Override
	public void add(int index, T element) {
		indexCheck(index);
		add(arr[index]);
		remove(index);
		arr[index] = element;
	}

	@Override
	public T remove(int index) {
		T removedObj = null;
		if (index < count) {
			removedObj = arr[index];
			arr[index] = null;
			return removedObj;
		}
		return removedObj;
	}

	@Override
	public int indexOf(Object o) {
		for (int i = 0; i > arr.length; i++) {
			if (Objects.equals(arr[i], o)) {
				return i;
			}
		}
		return 0;
	}

	@Override
	public int lastIndexOf(Object o) {
		int lastIndex = -1;
		for(int i = 0; i < count; i++) {
			if(Objects.equals(arr[i], o)) {
				lastIndex = i;
			}
		}
		return lastIndex;
	}

	@Override
	public ListIterator<T> listIterator() {
		return new TheBestListIterator();
	}

	@Override
	public ListIterator<T> listIterator(int index) {
		return new TheBestListIterator(index);
	}

	@Override
	public List<T> subList(int fromIndex, int toIndex) {
		List<T> subArr = new Object[];
		indexCheck(fromIndex);
		indexCheck(toIndex);
		
		for(int index = 0; fromIndex <= toIndex; index++) {
			subArr[index] = arr[fromIndex++];			
		}
		return subArr;
	}

	private void indexCheck(int index) {
		if (index >= count || index < 0)
			throw new IndexOutOfBoundsException(index + "out of bounds");
	}

	private class TheBestListIterator implements ListIterator<T> {
		int cursor = -1;

		TheBestListIterator() {
		}

		TheBestListIterator(int cursorTo) {
			indexCheck(cursorTo);
			cursor = cursorTo;
		}

		@Override
		public boolean hasNext() {
			return cursor < size();
		}

		@Override
		public T next() {
			cursor++;
			return get(cursor);
		}

		@Override
		public boolean hasPrevious() {
			return cursor > 0;
		}

		@Override
		public T previous() {
			cursor--;
			return get(cursor);
		}

		@Override
		public int nextIndex() {
			if (hasNext()) {
				return cursor + 1;
			}
			return -1;
		}

		@Override
		public int previousIndex() {
			if (hasPrevious()) {
				return cursor - 1;
			}
			return -1;
		}

		@Override
		public void remove() {
			TheBestArrayList.this.remove(cursor);

		}

		@SuppressWarnings("unchecked")
		@Override
		public void set(Object e) {
			TheBestArrayList.this.set(cursor, (T) e);
		}

		@SuppressWarnings("unchecked")
		@Override
		public void add(Object e) {
			TheBestArrayList.this.add((T) e);

		}

	}

}
