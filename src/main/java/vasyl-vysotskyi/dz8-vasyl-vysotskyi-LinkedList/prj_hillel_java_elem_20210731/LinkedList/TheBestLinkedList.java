package prj_hillel_java_elem_20210731.LinkedList;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.ListIterator;
import java.util.Objects;

public class TheBestLinkedList<E> implements List<E> {

	private static class Node<E> {
		private E data;
		private Node<E> next;
		private Node<E> prev;

		public Node() {
		}

		public Node(Node<E> prev, E data, Node<E> next) {
			this.data = data;
			this.next = next;
			this.prev = prev;
		}
	}

	private int count = 0;
	private Node<E> first;
	private Node<E> last;

	@Override
	public int size() {
		return count;
	}

	@Override
	public boolean isEmpty() {
		return size() == 0;
	}

	@Override
	public boolean contains(Object o) {
		for (int i = 0; i < size(); i++) {
			if (Objects.equals(get(i), o)) {
				return true;
			}
		}
		return false;
	}

	@Override
	public Iterator<E> iterator() {

		return new Iterator<E>() {
			Node<E> cursor = new Node<>(last, null, first);

			@Override
			public boolean hasNext() {
				return true;
			}

			@Override
			public E next() {
				cursor = cursor.next;
				return cursor.next.data;
			}
		};
	}

	@Override
	public Object[] toArray() {
		Object[] array = new Object[count];
		for (int i = 0; i < size(); i++) {
			array[i] = get(i);
		}
		return array;
	}

	@Override
	public <T> T[] toArray(T[] a) {
		T[] result = (T[]) new Object[size()];
		for (int i = 0; i < size(); i++) {
			result[i] = (T) get(i);
		}
		return result;
	}

	public void addFirst(E e) {
		Node<E> firstNode = first;
		Node<E> lastNode = last;
		Node<E> newNode = new Node<>(last, e, first);
		first = newNode;
		if (firstNode == null) {
			last = newNode;
		} else {
			firstNode.prev = newNode;
			lastNode.next = newNode;
		}
		count++;
	}

	public void addLast(E e) {
		Node<E> firstNode = first;
		Node<E> lastNode = last;
		Node<E> newNode = new Node<>(last, e, first);
		last = newNode;
		if (lastNode == null) {
			first = newNode;
			last.next = first;
			last.prev = first;
			first.next = last;
			first.prev = last;
		} else {
			firstNode.prev = newNode;
			lastNode.next = newNode;
		}
		count++;
	}

	@Override
	public boolean add(E e) {
		addLast(e);
		return true;
	}

	@Override
	public boolean remove(Object o) {
		Node<E> removedNode = first;
		Node<E> prevRemoved = removedNode.prev;
		Node<E> nextRemoved = removedNode.next;
		int iterations = 0;

		while (iterations <= size()) {
			if (Objects.equals(removedNode.data, o)) {
				prevRemoved.next = nextRemoved;
				nextRemoved.prev = prevRemoved;
				removeNode(removedNode);

				return true;
			}
			removedNode = removedNode.next;
			prevRemoved = prevRemoved.next;
			nextRemoved = nextRemoved.next;

			iterations++;
		}
		return false;
	}

	private void removeNode(Node<E> n) {
		Node<E> prev = n.prev;
		Node<E> next = n.next;
		prev.next = next;
		next.prev = prev;
		n.data = null;
		if (first.data == null) {
			first = next;
		} else if (last.data == null) {
			last = prev;
		}
		count--;
	}

	@Override
	public boolean containsAll(Collection<?> c) {
		boolean isContains = true;
		for (Object obj : c) {
			isContains &= contains(obj);
		}
		return isContains;
	}

	@Override
	public boolean addAll(Collection<? extends E> c) {
		for (E obj : c) {
			add(obj);
		}

		return true;
	}

	@Override
	public boolean addAll(int index, Collection<? extends E> c) {
		throw new UnsupportedOperationException();
	}

	@Override
	public boolean removeAll(Collection<?> c) {
		for (Object obj : c) {
			remove(obj);
		}
		return false;
	}

	@Override
	public boolean retainAll(Collection<?> c) {
		throw new UnsupportedOperationException();
	}

	@Override
	public void clear() {
		first = null;
		last = null;
	}

	@Override
	public E get(int index) {
		Node<E> nodeToGet = first;
		if (index < size()) {
			for (int i = 0; i <= index; i++) {
				if (i == index) {
					return nodeToGet.data;
				}
				nodeToGet = nodeToGet.next;
			}
		}
		return null;
	}

	public E getFirst() {
		if (Objects.equals(first, null)) {
			throw new NullPointerException("Nods is null, addNoda to create first!");
		}
		return first.data;
	}

	public E getLast() {
		if (Objects.equals(last, null)) {
			throw new NullPointerException("Nods is null, addNoda to create last!");
		}
		return last.data;
	}

	private Node<E> getNodeToIndex(int index) {
		Node<E> node = first;
		for (int i = 0; i < index; i++) {
			node = node.next;
		}
		return node;
	}

	private Node<E> getNodeToData(E e) throws Exception {
		Node<E> node = first;
		if (contains(e)) {
			for (int i = 0; i < size(); i++) {
				if (Objects.equals(get(i), e)) {
					return node;
				}
				node = node.next;
			}
		} else {
			throw new Exception("Noda dosn't exist!");
		}
		return null;
	}

	@Override
	public E set(int index, E e) {
		E data = getNodeToIndex(index).data;
		getNodeToIndex(index).data = e;
		return data;
	}

	@Override
	public void add(int index, E element) {
		try {
			indexCheck(index);
		} catch (Exception e) {
			e.printStackTrace();
		}

		if (!element.equals(null)) {
			Node<E> current = getNodeToIndex(index);
			Node<E> prev = current.prev;

			Node<E> newNode = new Node<>(prev, element, current);
			prev.next = newNode;
			current.prev = newNode;
			if (index == 0) {
				first = newNode;
			} else if (index == size() - 1) {
				last = newNode;
			}
			count++;
		}
	}

	private void indexCheck(int index) throws Exception {
		if (!(index < size() && index >= 0)) {
			throw new Exception("out of bounds! - " + index);
		}
	}

	@Override
	public E remove(int index) {
		Node<E> getRemove = getNodeToIndex(index);
		removeNode(getRemove);
		return getRemove.data;
	}

	@Override
	public int indexOf(Object o) {
		for (int i = 0; i < size(); i++) {
			if (Objects.equals(getNodeToIndex(i), o)) {
				return i;
			}
		}
		return -1;
	}

	public int getIndexOfNode(Node<E> node) {
		Node<E> myNode = first;
		for (int i = 0; i < size(); i++) {
			if (Objects.equals(node, myNode)) {
				return i;
			}
			myNode = myNode.next;
		}
		return -1;
	}

	@Override
	public int lastIndexOf(Object o) {
		for (int i = size() - 1; i >= 0; i--) {
			if (Objects.equals(getNodeToIndex(i), o)) {
				return i;
			}
		}
		return -1;
	}

	@Override
	public ListIterator<E> listIterator() {
		return new LinkedListIterator<>();
	}

	@Override
	public ListIterator<E> listIterator(int index) {
		return new LinkedListIterator<>(index);
	}

	public class LinkedListIterator<E> extends Iterator implements ListIterator {
		public LinkedListIterator() {
			this(-1);
		}

		public LinkedListIterator(int index) {
			this.index = index;
		}

		@Override
		public boolean hasPrevious() {
			return true;
		}

		@Override
		public int nextIndex() {
			int index = getIndexOfNode(curent);
			if (index == size() - 1) {
				return 0;
			} else {
				return getIndexOfNode(curent) + 1;
			}

		}

		@Override
		public int previousIndex() {
			int index = getIndexOfNode(curent);
			if (index == 0) {
				return size() - 1;
			} else {
				return getIndexOfNode(this.curent) - 1;
			}
		}

		@Override
		public void remove() {
			removeNode(curent);
		}

		@Override
		public void set(Object e) {
			curent.data = e;
		}

		@Override
		public void add(Object e) {
			throw new UnsupportedOperationException();
		}

	}

	public class Iterator<E> implements java.util.Iterator {

		Node<E> curent = null;
		int index = -1;

		@Override
		public boolean hasNext() {
			return true;
		}

		@Override
		public E next() {
			if (index >= size()) {
				System.out.println("get round");
				index = 0;
			}
			E data;
			if (index == -1) {
				data = (E) first.data;
				curent = (Node<E>) first;
			} else {
				data = curent.data;
				curent = curent.next;
			}
			index++;
			return data;

		}

		public E previous() {
			if (index < -1 * size()) {
				System.out.println("get round");
				index = 0;
			}
			E data;
			if (index == -1) {
				data = (E) first.data;
				curent = (Node<E>) first;
			} else {
				data = curent.data;
				curent = curent.prev;
			}
			index--;
			return data;
		}

	}

	@Override
	public List<E> subList(int fromIndex, int toIndex) {
		try {
			indexCheck(fromIndex);
			indexCheck(toIndex);
		} catch (Exception e) {
			e.printStackTrace();
		}
		List<E> list = new ArrayList<>();
		for (; fromIndex <= toIndex; fromIndex++) {
			list.add(get(fromIndex));
		}
		return list;
	}

}
