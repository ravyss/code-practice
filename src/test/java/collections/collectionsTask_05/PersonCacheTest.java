package collections.collectionsTask_05;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertSame;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import collections.collectionsTask_01.Person;

public class PersonCacheTest {
	private PersonCache personCache;

	@Test
	public void AddPerson_AddedCorrectPerson_True() {
		// given
		long id = 0;
		Person personToSave = new Person(id);

		// when
		personCache.addPerson(personToSave);
		Person personToGet = personCache.getPersonById(id);

		// then
		assertNotNull(personToGet);
		assertSame(id, personToGet.getId());

	}

	@Test
	public void AddPerson_AddedNullPerson_False() {
		// given
		long id = 0;
		Person personToSave = null;

		// when
		personCache.addPerson(personToSave);
		Person personToGet = personCache.getPersonById(id);

		// then
		assertNotNull(personToGet);
		assertSame(Person.NULL_PERSON, personToGet);
		assertSame(Person.NULL_PERSON.getId(), personToGet.getId());
		assertFalse(id == personToGet.getId());

	}

	@Test
	public void RemovePersonById_RemoveToCorrectPerson_True() {
		// given
		long id = 0;
		Person personToSave = new Person(id);

		// when
		personCache.addPerson(personToSave);
		Person existingPerson = personCache.getPersonById(id);
		personCache.removePersonById(id);
		Person personToGet = personCache.getPersonById(id);

		// then
		assertNotNull(personToGet);
		assertSame(Person.NULL_PERSON, personToGet);
		assertNotEquals(existingPerson, personToGet);

	}

	@Before
	public void before() {
		personCache = new PersonCache();
	}

	@After
	public void after() {
		personCache = null;
	}
}
