package collections.collectionsTask_05;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import collections.collectionsTask_01.Person;

public class PersonServiceTeest {
	private PersonServiceImpl personService;
	static final long ID = 0;
	private PersonCache mockCache = mock(PersonCache.class);
	

	
	@Test
	public void ChangeAge_CorrectAge_True() {
		//given
		short age = 22;
		Person person = Person.createPerson(ID, (short)47, "NAme", "surname");
		
		whenCorrectVaribls(person);	
		
		//when		
		boolean isChanged = personService.changeAge(ID, age);
		
		//then
		Person thenPerson = Person.createPerson(ID, age, person.getFirstName(), person.getSurname());
		thenInChangeTest(1, isChanged, thenPerson);	
				
	}
	
	@Test
	public void ChangeAge_SameAge_False() {
		//given
		short age = 22;
		Person person = Person.createPerson(ID, age, "NAme", "surname");
		when(mockCache.getPersonById(ID)).thenReturn(person);
		
		//when
		boolean isChanged = personService.changeAge(ID, age);
		
		//then
		thenInChangeSameTest(isChanged);
	}
	
	@Test
	public void ChangeAge_UncorrectAge_False() {
		//given
		short age = -1;
		Person person = Person.createPerson(ID, (short)22, "NAme", "surname");
		when(mockCache.getPersonById(ID)).thenReturn(person);
		
		//when		
		boolean isChanged = personService.changeAge(ID, age);
		
		//then
		Person thenPerson = Person.createPerson(ID, age, person.getFirstName(), person.getSurname());
		thenInChangeTest(0, !isChanged, thenPerson);
	}
	
	@Test
	public void ChangeName_CorrectName_True() {
		//given
		String name = "Test";
		Person person = Person.createPerson(ID, (short)47, "NAme", "surname");
		
		whenCorrectVaribls(person);
		
		//when

		boolean isChanged = personService.changeName(ID, name);
		
		//then
		Person thenPerson = Person.createPerson(ID, person.getAge(), name, person.getSurname());
		thenInChangeTest(1, isChanged, thenPerson);
		
	}
	
	@Test
	public void ChangeName_NameAreSame_False() {
		//given
		String name = "Test";
		Person person = Person.createPerson(ID, (short)22, name, "surname");
		when(mockCache.getPersonById(ID)).thenReturn(person);
		
		
		//when
		boolean isChanged = personService.changeName(ID, name);
		
		//then
		thenInChangeSameTest(isChanged);
	}
	
	@Test
	public void ChangeName_NameAreNull_False() {
		//given
		String name = null;
		Person person = Person.createPerson(ID, (short)47, "NAme", "surname");
		
		when(mockCache.getPersonById(ID)).thenReturn(person);
		
		//when

		boolean isChanged = personService.changeName(ID, name);
		
		//then
		Person thenPerson = Person.createPerson(ID, person.getAge(), name, person.getSurname());
		thenInChangeTest(0, !isChanged, thenPerson);
	}
	
	
	@Test
	public void ChangeSurame_CorrectSurname_True() {
		//given
		String surname = "Testovich";
		Person person = Person.createPerson(ID, (short)47, "NAme", "surname");
		
		whenCorrectVaribls(person);	
		
		//when

		boolean isChanged = personService.changeSurname(ID, surname);
		
		//then
		Person thenPerson = Person.createPerson(ID, person.getAge(), person.getFirstName(), surname);
		thenInChangeTest(1, isChanged, thenPerson);
	}
	
	@Test
	public void ChangeSurname_SurnameAreSame_False() {
		//given
		String surname = "Testovich";
		Person person = Person.createPerson(ID, (short)22, "NAme", surname);
		when(mockCache.getPersonById(ID)).thenReturn(person);
		
		//when
		boolean isChanged = personService.changeSurname(ID, surname);
		
		//then
		thenInChangeSameTest(isChanged);
	}
	
	@Test
	public void ChangeSurname_SurnameAreNull_False() {
		//given
		String surname = null;
		Person person = Person.createPerson(ID, (short)47, "NAme", "surname");
		
		when(mockCache.getPersonById(ID)).thenReturn(person);
		
		//when

		boolean isChanged = personService.changeSurname(ID, surname);
		
		//then
		Person thenPerson = Person.createPerson(ID, person.getAge(), person.getFirstName(), surname);
		thenInChangeTest(0, !isChanged, thenPerson);
	}
	

	
//	@Test 
//	public void Update_CorrectVarible_True() {
//		//given
//		Person person = Person.createPerson(ID, (short)47, "NAme", "surname");
//		String surname = "Testovich";
//		String name = "Test";
//		short age = 22;
//		when(mockCache.removePersonById(ID)).thenReturn(true);
//		when(mockCache.addPerson(person)).thenReturn(true);	
//		
//		//when
//		boolean isUpdated = personService.update(ID, age, name, surname);
//		
//		//then
//		Person personThen = Person.createPerson(ID, age, name, surname);
//		thenInUptadeTest(1, isUpdated, personThen);
//	}
//	
//	@Test 
//	public void Update_UncorrectVarible_False() {
//		//given
//		Person.createPerson(ID, (short)47, "name", "surname");
//		String surname = null;
//		String name = null;
//		short age = -1;
//		
//		//when
//		boolean isUpdated = personService.update(ID, age, name, surname);
//		
//		//then
//		Person person = Person.createPerson(ID, age, name, surname);
//		thenInUptadeTest(0, !isUpdated, person);
//	}
	
	public void whenCorrectVaribls(Person person) {
		when(mockCache.getPersonById(ID)).thenReturn(person);
		when(mockCache.removePersonById(ID)).thenReturn(true);
		when(mockCache.addPerson(person)).thenReturn(true);	
	}
	
	public void thenInChangeSameTest(boolean isChanged) {
		verify(mockCache, times(1)).getPersonById(ID);
		assertFalse(isChanged);
	}
	
	public void thenInChangeTest(int times, boolean isChanged, Person person) {
		verify(mockCache, times(1)).getPersonById(ID);
		verify(mockCache, times(times)).removePersonById(ID);
		verify(mockCache, times(times)).addPerson(person);
		assertTrue(isChanged);
	}
	
//	public void thenInUptadeTest(int times, boolean isUpdated, Person person) {
//		verify(mockCache, times(times)).removePersonById(ID);
//		verify(mockCache, times(times)).addPerson(person);
//		assertTrue(isUpdated);
//	}
	

	@Before
	public void before() {
		personService = new PersonServiceImpl(mockCache);
	}
	
	@After
	public void after() {
		personService = null;
		mockCache = mock(PersonCache.class);
	}
	
	
}
